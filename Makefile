all:
tag:
	git tag -s $(shell date +%Y%m%dT%H%M%SZ -u)

# This is a helper for infra
update-puppet-to-tag:
	sed -i -e "/.revision = /s/20.*[0-9]Z/$(shell git tag |grep 20 |tail -n1)/" ../puppet/dist/mirrors/manifests/scripts.pp
