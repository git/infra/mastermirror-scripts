#!/bin/sh
# Copyright 2012-2015 Gentoo Authors; Distributed under the GPL v2

# this is ugly and I don't like it

if [[ $HOSTNAME == TODO ]]; then
	/bin/date -u +'%s %a, %d %b %Y %H:%M:%S %z' > \
		/var/tmp/gmirror/experimental/.timestamp-experimental.x
	/bin/date -u +'%s %a, %d %b %Y %H:%M:%S %z' > \
		/var/tmp/gmirror/releases/.timestamp-releases.x
fi
if [[ $HOSTNAME == dipper ]]; then
	/bin/date -u +'%s %a, %d %b %Y %H:%M:%S %z' > \
		/var/tmp/gmirror-releases/experimental/.timestamp-experimental.x
	/bin/date -u +'%s %a, %d %b %Y %H:%M:%S %z' > \
		/var/tmp/gmirror-releases/releases/.timestamp-releases.x
fi
