#!/bin/bash
# Copyright 2014-2020 Gentoo Authors; Distributed under the GPL v2

DATADIR="/data/gmirror-distfiles"
LOGDIR="${DATADIR}/log"
RSYNC="/usr/bin/rsync"
RSYNC_ARGS="--no-motd --recursive --times --links --ignore-errors --timeout=300 --quiet"
RSYNC_ARGS_DELETE="--delete --delete-after"
[[ -d ${LOGDIR} ]] || mkdir ${LOGDIR}

# get the helper files
${RSYNC} ${RSYNC_ARGS} ${RSYNC_ARGS_DELETE} \
	woodpecker.gentoo.org::distfiles-local/ \
	${DATADIR}/distfiles-local
# Do NOT delete old whitelist entries; they must explicitly be set to empty if
# you want to remove them.
${RSYNC} ${RSYNC_ARGS} \
	woodpecker.gentoo.org::distfiles-whitelist/ \
	${DATADIR}/distfiles-whitelist
# Copy distfiles-local files to distfiles dir so that emirrordist can process
# them
# Do NOT --delete, let emirrordist handle the deletion logic
# TODO: we could make this a hardlink for speed?
${RSYNC} ${RSYNC_ARGS} \
	${DATADIR}/distfiles{-local/*,}

# build one master file due to bug 500030
cat $(/usr/bin/find ${DATADIR}/distfiles-whitelist -type f ) > \
	${DATADIR}/tmp/whitelist-master.txt

# Clear out the success/fail.log so that we can report on it
# (emirrordist appends)
> ${LOGDIR}/failure.log
if [[ $(/bin/date '+%k') -eq 0 ]]; then
	> ${LOGDIR}/success.log
fi

# human readable time format, 1814400 seconds equals 3 weeks
DELAY=1814400
# WARNING: This is sensitive to *DBM that is built with python
/usr/bin/emirrordist \
	--distfiles=${DATADIR}/distfiles/ \
	--delete --jobs=10 --repo=gentoo \
	--deletion-delay=${DELAY} \
	--failure-log=${LOGDIR}/failure.log \
	--success-log=${LOGDIR}/success.log \
	--scheduled-deletion-log=${LOGDIR}/deletion.log \
	--deletion-db=${LOGDIR}/deletion-db.gdbm \
	--distfiles-db=${LOGDIR}/distfile-db.gdbm \
	--recycle-db=${LOGDIR}/recycle-db.gdbm \
	--recycle-dir=${DATADIR}/distfiles-archive/ \
	--temp-dir=${DATADIR}/tmp/ \
	--whitelist-from=${DATADIR}/tmp/whitelist-master.txt \
	--distfiles-local=${DATADIR}/distfiles-local \
	--symlinks \
	--layout-conf=/usr/local/bin/mastermirror/layout.conf.new \
	--mirror

INDEXNAME=.INDEX.$(date -u +%Y%m%d%H%M%S).txt
INDEXFILE=${DATADIR}/${INDEXNAME}
( cd ${DATADIR}/distfiles && find [0-9a-f][0-9a-f] -mindepth 1 ) |
	sort >"${INDEXFILE}.tmp" && \
	mv "${INDEXFILE}.tmp" "${INDEXFILE}" && \
	ln -sf "${INDEXNAME}" "${DATADIR}"/INDEX && \
	find "$DATADIR" \
		-maxdepth 1 \
		-mindepth 1 \
		-name '.INDEX.*.txt' \
		! -mtime -7 \
		-delete

/bin/date -u '+%s' > ${DATADIR}/distfiles/timestamp.mirmon
