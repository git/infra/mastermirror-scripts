#!/bin/bash
# Copyright 2011-2015 Gentoo Authors; Distributed under the GPL v2

h=$(hostname --fqdn |cut -d. -f1)

FINALDIR="/data/mirror"
PASSWD_FILE=""
RSYNC="/usr/bin/rsync"
RSYNC_ARGS="--recursive --links --perms --times --delete --hard-links --no-motd --timeout=300 ${PASSWD_FILE:+--password-file }${PASSWD_FILE}"
RSYNC_ARGS+=" --quiet"

case $h in
	kestrel)
		# kestrel is space constrained, and the binpackages can be re-generated.
		# Saves 100+ GB
		RSYNC_ARGS+=" --exclude binpackages "
		# kestrel is space constrained, and the snapshot squashfs are a nice-to-have:
		# Saves 18 GB
		RSYNC_ARGS+=" --exclude squashfs "
		;;
esac

module=releases
${RSYNC} ${RSYNC_ARGS} masterreleases.gentoo.org::${module}/ ${FINALDIR}/${module}/
