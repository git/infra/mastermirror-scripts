#!/bin/bash
# Copyright 2011-2015 Gentoo Authors; Distributed under the GPL v2

FINALDIR="/var/tmp/gmirror-releases/experimental"
RSYNC="/usr/bin/rsync"
RSYNC_ARGS="--no-motd --recursive --times --links --ignore-errors --delete --delete-after --timeout=300 --exclude=timestamp*"
RSYNC_ARGS="${RSYNC_ARGS} --quiet"

[[ -d $FINALDIR ]] || mkdir $FINALDIR
${RSYNC} ${RSYNC_ARGS} woodpecker.gentoo.org::experimental-local/ ${FINALDIR}/


/bin/date -u '+%s %c' > ${FINALDIR}/timestamp.x
/bin/date -R -u > ${FINALDIR}/timestamp.chk
/bin/date -u '+%s' > ${FINALDIR}/timestamp.mirmon
