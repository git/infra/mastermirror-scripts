#!/bin/bash
# Copyright 2014-2015 Gentoo Authors; Distributed under the GPL v2

DATADIR="/data/gmirror-distfiles"
SCRIPTDIR="/usr/local/bin/mastermirror/"
LOGDIR="${DATADIR}/log"
REPORTS="${DATADIR}/reports"
[[ -d ${REPORTS} ]] || mkdir $REPORTS

${SCRIPTDIR}/gen-report-xml.py ${LOGDIR}/failure.log \
	${LOGDIR}/success.log ${LOGDIR}/deletion.log '%Y-%m-%d' \
	${DATADIR}/distfiles ${REPORTS}/failure.xml
${SCRIPTDIR}/gen-report-whitelist-xml.py ${DATADIR}/distfiles-whitelist \
	${REPORTS}/whitelists.xml
