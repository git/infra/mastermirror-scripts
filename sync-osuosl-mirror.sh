#!/bin/bash
# Copyright 2011-2015 Gentoo Authors; Distributed under the GPL v2

FINALDIR="/data/mirror"
PASSWD_FILE="/etc/mastermirror-fetch/osuosl-rsync.passwd"
RSYNC="/usr/bin/rsync"
RSYNC_ARGS="--recursive --links --perms --times --delete --hard-links --no-motd --timeout=300 --password-file ${PASSWD_FILE}"
RSYNC_ARGS="${RSYNC_ARGS} --quiet"

# Reminder: "masterdistfiles.g.o" is OSUOSL, hence the special user
${RSYNC} ${RSYNC_ARGS} gentoo@masterdistfiles.gentoo.org::gentoo/ ${FINALDIR}/
