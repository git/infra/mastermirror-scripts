#!/bin/bash
# Copyright 2011-2016 Gentoo Authors; Distributed under the GPL v2

umask 022 # ensure globally readable perms on new files for rsyncd

source /usr/local/bin/mastermirror/rsync-gen.vars

# The only time ${STAGEDIR_repo_gentoo}/metadata should not exist is when rsync-gen.sh
# hasn't ran even once yet
if [[ -d ${STAGEDIR_repo_gentoo}/metadata ]]; then
	/bin/date -u '+%s %c' > ${STAGEDIR_repo_gentoo}/metadata/timestamp.x.tmp
	/bin/mv ${STAGEDIR_repo_gentoo}/metadata/timestamp.x.tmp ${STAGEDIR_repo_gentoo}/metadata/timestamp.x
fi

if [[ -d ${FINALDIR_repo_gentoo}/metadata ]]; then
	/bin/date -R -u > ${FINALDIR_repo_gentoo}/metadata/timestamp.chk.tmp
	/bin/mv ${FINALDIR_repo_gentoo}/metadata/timestamp.chk.tmp ${FINALDIR_repo_gentoo}/metadata/timestamp.chk
fi
