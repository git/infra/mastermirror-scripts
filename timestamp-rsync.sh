#!/bin/sh
# Copyright 2012-2018 Gentoo Authors; Distributed under the GPL v2
case $HOSTNAME in
	kookaburra) TARGET=/var/tmp/gmirror/snapshots-final/.timestamp-snapshots.x ;;
	dipper) TARGET=/var/tmp/gmirror-rsync/snapshots-final/.timestamp-snapshots.x ;;
	blackcap) TARGET=/var/tmp/gmirror-rsync/snapshots-final/.timestamp-snapshots.x ;;
	*) echo "Unknown host in timestamp-rsync.sh!" 1>&2 ; exit 1 ;;
esac
/bin/date -u +'%s %a, %d %b %Y %H:%M:%S %z' >"${TARGET}"
