#!/usr/bin/env python
# Copyright 2011-2015 Gentoo Authors; Distributed under the GPL v2

import os,sys
if len(sys.argv) != 4:
	print("need 3 args, first being directory to scan, second being latest, third being it's size")
	sys.exit(1)
	
path=sys.argv[1]
maxsize=int(sys.argv[3])
if len(sys.argv[2]) != 8:
	print("date arg must be YYYYMMDD")
	sys.exit(1)
root = sys.argv[2]

if not hasattr(__builtins__, "set"):
	from sets import Set as set
file_list = [x for x in os.listdir(path) if os.path.isfile(os.path.join(path, x))]
pref_name="snapshot-"
post_name=".patch.bz2"
deltas = [x for x in file_list if x.startswith(pref_name) and x.endswith(post_name)]
chains={}
for x in deltas:
	try:
		start, end = x[len(pref_name):-len(post_name)].split("-")
	except ValueError:
		continue
	if end in chains:
		print("warning, end %s already exists; this is a bit weird" % end)
		continue
	chains[end] = start	

save_files = set()
node = root
count = 0
while node in chains:
	s="%s%s-%s%s" % (pref_name, chains[node], node, post_name)
	count += os.stat(os.path.join(path, s)).st_size
	if count > maxsize:
		break
	save_files.update([s, s+".md5sum", s+".gpgsig"])
	node = chains[node]

for x in file_list:
	if x not in save_files:
		print("nuking %s" % x)
		try:
			os.unlink(os.path.join(path,x))
		except OSError as oe:
			print("failed removing %s cause of %s" % (os.path.join(path, x), str(oe)))
