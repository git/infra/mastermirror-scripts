#!/bin/bash
# Copyright 2011-2015 Gentoo Authors; Distributed under the GPL v2

FINALDIR="/data/mirror"
PASSWD_FILE=""
RSYNC="/usr/bin/rsync"
RSYNC_ARGS="--recursive --links --perms --times --delete --hard-links --no-motd --timeout=300 ${PASSWD_FILE:+--password-file }${PASSWD_FILE}"
RSYNC_ARGS="${RSYNC_ARGS} --quiet"

module=distfiles
${RSYNC} ${RSYNC_ARGS} masterreleases.gentoo.org::${module}/ ${FINALDIR}/${module}/
