#!/usr/bin/env python
# Copyright 2014-2015 Gentoo Authors; Distributed under the GPL v2

from __future__ import unicode_literals

import io
import os
import sys
import time
from xml.sax.saxutils import escape as escape_xml

def grab_whitelists(whitelists_dir):
	whitelists = {}
	for x in os.listdir(whitelists_dir):
		if x[:1] == ".":
			continue
		x = os.path.join(whitelists_dir, x)
		if not os.path.isfile(x):
			continue
		whitelists[x] = []
		with io.open(x, mode='r', encoding='utf_8') as f:
			for entry in f:
				entry = entry.lstrip().rstrip()
				if len(entry) == 0 or entry.startswith("#"):
					continue
				whitelists[x].append(entry.lstrip().rstrip())

		if not whitelists[x]:
			del whitelists[x]
	return whitelists

def write_report(whitelists, outf):
	outf.write("""<?xml version='1.0'?>
	<?xml-stylesheet href="/xsl/guide.xsl" type="text/xsl"?>
	<guide link="failure.xml">
	<title>Distfiles Mirroring Whitelist Report</title>
	<version>1.0</version>
	<date>"""+time.asctime(time.localtime())+"""</date>
	""")
	outf.write("<chapter><title>White Lists</title>")
	if not whitelists:
		outf.write("<section><body><p>No whitelists.</p></body></section>")
	for x in sorted(whitelists):
		outf.write("<section><title>%s</title><body><ul>\n" % os.path.basename(x))
		whitelists[x].sort()
		for y in whitelists[x]:
			outf.write("  <li>%s</li>\n" % escape_xml(y))
		outf.write("</ul></body></section>\n")
	outf.write("</chapter></guide>")
	outf.close()

def usage():
	sys.stderr.write("usage: %s <whitelists dir> <output file>\n" % \
		os.path.basename(sys.argv[0]))
	sys.stderr.flush()

def main(argv):
	if len(argv) != 3 or \
		not os.path.isdir(argv[1]) or \
		not os.path.isdir(os.path.dirname(argv[2])) or \
		not os.access(os.path.dirname(argv[2]), os.W_OK):
		usage()
		return 1
	with io.open(argv[2], mode='w', encoding="utf_8") as outf:
		whitelists = grab_whitelists(argv[1])
		write_report(whitelists, outf)
	return os.EX_OK

if __name__ == "__main__":
	sys.exit(main(sys.argv))
