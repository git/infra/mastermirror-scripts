#!/bin/bash
# Copyright 2011-2023 Gentoo Foundation; Distributed under the GPL v2
# might be earlier copyright, no history available

# Keep this variable in sync in both sign-autobuilds.sh & sync-autobuilds.sh
_ARCHES="alpha amd64 arm64 arm hppa ia64 loong m68k mips ppc riscv s390 sh sparc x86"
        #alpha amd64 arm64 arm hppa ia64 loong m68k mips ppc riscv s390 sh sparc x86
ARCHES=${ARCHES:-${_ARCHES}}

# The -rp_*asc filter rules causes rsync to only delete .asc files that are in
# directories that would otherwise be deleted.
RSYNC_OPTS=(
	-aO
	--hard-links
	--acls
	--xattrs
	--password-file=/etc/mastermirror-fetch/masterdistfiles-rsync.passwd
	--delete
	--delete-delay
	--timeout=600
	--exclude='.*'
	--filter=-rp_*.asc
	--no-motd
	# arm has ~111 files in a daily build as of ./arm/autobuilds/20230828T171640Z/
	# so this needs to be at least that high.
	--max-delete=150
	# If the local side is nwer, don't replace
	# should happen with the .DIGESTS & .sha256 files
	--update
)

# Do NOT expand the following yet
#_SRC='masterdistfiles@poseidon.amd64.dev.gentoo.org::weekly/${ARCH}/'
#_SRC='masterdistfiles@skimmer.gentoo.org::weekly/${ARCH}/'
#_SRC='masterdistfiles@nightheron.gentoo.org::weekly/${ARCH}/'
_SRC='masterdistfiles@releng-incoming.gentoo.org::weekly/${ARCH}/'
_DST_BASE='/var/tmp/gmirror-releases/releases/'
_DST_BACKUP_BASE='/var/tmp/gmirror-releases/releases-backup/'
# compat
if [[ $HOSTNAME == TODO ]]; then
	_DST_BASE='/var/tmp/gmirror/releases/'
	_DST_BACKUP_BASE='/var/tmp/gmirror/releases/releases-backup/'
fi
[[ -d $_DST_BASE ]] || mkdir -p $_DST_BASE
[[ -d $_DST_BACKUP_BASE ]] || mkdir -p $_DST_BACKUP_BASE
# No expansion is IMPORTANT
_DST=${_DST_BASE}/'${ARCH}'/autobuilds
_DST_BACKUP=${_DST_BACKUP_BASE}/'${ARCH}'/autobuilds

DEBUG=''
VERBOSE=''
# Nothing to edit beyond this point
DEBUGP=
VERBOSEP=
[ -n "$DEBUG" ] && DEBUGP="echo"
[ -n "$DEBUG" ] && RSYNC_OPTS+=( -n )
if [ -n "$VERBOSE" ]; then
	RSYNC_OPTS+=( -v )
else
	RSYNC_OPTS+=( -q )
fi

for ARCH in $ARCHES ; do
	src="$(eval echo $_SRC)"
	dst="$(eval echo $_DST)"
	dst_backup="$(eval echo $_DST_BACKUP)"
	$DEBUGP mkdir -pv "$dst"
	rsync \
		"${RSYNC_OPTS[@]}" \
		--backup \
		--backup-dir="$dst_backup" \
		"$src" \
		"$dst"
done
