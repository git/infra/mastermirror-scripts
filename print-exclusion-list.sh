#!/bin/bash
# Copyright 2017 Gentoo Authors; Distributed under the GPL v2
#
# This prints an exclusion list of content that should not be included in
# snapshot tarballs or similar output.
#
# app-bar/ChangeLog is a valid package name, so we have to be careful about it!
#
# This should exclude:
# - ChangeLogs (not not packages named ChangeLog)
# - .checksum-test-marker

SRCDIR="$1"
if [ -z "${SRCDIR}" ]; then
  echo "Usage: $(basename $0) DIR" 1>&2
  exit 2
fi
if [ ! -d "${SRCDIR}" ]; then
  echo "${SRCDIR} is not a directory" 1>&2
  exit 1
fi
if [ ! -e "${SRCDIR}/profiles/repo_name" ]; then
  echo "${SRCDIR} is probably not a portdir or overlay, missing profiles/repo_name" 1>&2
  exit 1
fi

find "${SRCDIR}" \
	\( \
		-type f \
		-regextype posix-egrep \
		\( \
			-path "${SRCDIR}/eclass/ChangeLog*" -o \
			-path "${SRCDIR}/profiles/ChangeLog*" -o \
			-path "${SRCDIR}/profiles/*/ChangeLog*" -o \
			-regex "${SRCDIR}/[^/]+/[^/]+/ChangeLog(-[0-9]+)?$" \
		\) \
	\) \
	-o \
	-name '.checksum-test-marker' \
	\
	| sed "s,${SRCDIR}/*,,g" \
	\
	| sort

# vim:ft=sh noet ts=2 sts=2:
